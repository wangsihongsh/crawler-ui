# 简单爬虫demo
1. JDK1.8
2. springboot 2.1.5
3. webmagic 0.7.3
4. mongodb 4.0.2
5. swagger2 2.9.2
## 效果图
![](readme_files/3.jpg)
![](readme_files/4.jpg)
![](readme_files/5.jpg)
![](readme_files/1.jpg)
![](readme_files/2.jpg)
## MongoDB简单语法
**查看版本信息**
> db.version()  

**查看所有数据库**
> show dbs

切换数据库
use [db_name]

查看当前数据库
db

删除当前数据库
db.dropDatabase()

查看集合
show collections

删除集合
db.test.drop()

插入文档
db.test.insert([
{name:"测试1"},
{name:"测试2"}
])

删除文档
db.test.remove({})
db.test.remove({name:"测试2"})

查询文档
db.test.find()
db.test.find({name:"测试2"})

**pom.xml**
```
<!-- API swagger2 -->
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger2</artifactId>
	<version>2.9.2</version>
</dependency>
<!-- API swagger2 ui-->
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger-ui</artifactId>
	<version>2.9.2</version>
</dependency>
<!-- webmagic核心包 -->
<dependency>
	<groupId>us.codecraft</groupId>
	<artifactId>webmagic-core</artifactId>
	<version>0.7.3</version>
	<exclusions>
		<exclusion>
			<artifactId>slf4j-log4j12</artifactId>
			<groupId>org.slf4j</groupId>
		</exclusion>
	</exclusions>
</dependency>
<!-- webmagic扩展包 -->
<dependency>
	<groupId>us.codecraft</groupId>
	<artifactId>webmagic-extension</artifactId>
	<version>0.7.3</version>
</dependency>
<!--lang3工具包-->
<dependency>
	<groupId>org.apache.commons</groupId>
	<artifactId>commons-lang3</artifactId>
	<version>3.9</version>
</dependency>
<!--mongodb-->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
<!--thymeleaf-->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<scope>runtime</scope>
</dependency>
<dependency>
	<groupId>org.projectlombok</groupId>
	<artifactId>lombok</artifactId>
	<optional>true</optional>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-test</artifactId>
	<scope>test</scope>
</dependency>
```

**application.yml**
```
spring:
  data:
    mongodb:
      host: 127.0.0.1
      database: crawler
```

**MySwagger2Config**
```
@Configuration
public class MySwagger2Config {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.crawler"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SpringBoot利用swagger2构建api文档")
                .description("简单优雅的restfun风格，http://localhost:8080/swagger-ui.html")
                .termsOfServiceUrl("http://localhost:8080/druid/index.html")
                .version("1.0")
                .build();
    }
}
```

**启动类**
```
/**
 * @author rezero
 * @EnableScheduling  开启定时任务
 * @SpringBootApplication  启动类注解
 * @EnableSwagger2 开启swagger注解
 */
@EnableSwagger2
@EnableScheduling
@SpringBootApplication
public class CrawlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrawlerApplication.class, args);
    }

}
```

**以笔趣阁为例，NovelProcessor**
```
@Component
public class NovelProcessor implements PageProcessor {

    private Logger logger = LoggerFactory.getLogger(NovelProcessor.class);

    @Autowired
    private NovelPipeline novelPipeline;

    /**
     * 起始url地址
     */
    private String url = "https://www.biquge5.com/shuku/";

    private Site site = Site.me()
            //设置编码
            // .setCharset("utf8")
            .setCharset("gbk")
            //设置超时时间
            .setTimeOut(10 * 1000)
            //设置重试间隔时间
            .setRetrySleepTime(3000)
            //设置重试次数
            .setRetryTimes(3)
            //设置用户代理（操作系统及版本、CPU 类型、浏览器及版本、浏览器渲染引擎、浏览器语言、浏览器插件等）
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");

    @Override
    public void process(Page page) {
        //解析页面（获取小说名字，小说详情URL）
        List<Selectable> nodes = page.getHtml().css("ul.list-top div.col-md-5 a").nodes();
        if (nodes.size() != 0) {
            //如果集合不为空(小说列表页)
            nodes.forEach(item -> {
                //把获取的url地址放入任务队列（小说详情页）
                page.addTargetRequest(item.links().toString());
            });
            //将下一页放入任务队列（小说列表页）
            List<Selectable> list = page.getHtml().css("div.page a:nth-child(3) ").nodes();
            page.addTargetRequest(list.get(0).links().toString());
        } else {
            //如果集合为空(小说详情页)
            List<Selectable> chapters = page.getHtml().css("div#list a").nodes();
            chapters.forEach(item -> {
                //把获取的url地址放入任务队列(小说章节页)
                page.addTargetRequest(item.links().toString());
            });
            if (chapters.size() != 0) {
                //(小说详情页)
                String image = page.getHtml().css("div#fmimg img", "src").toString();
                String title = page.getHtml().css("h1", "text").toString();
                String autor = page.getHtml().css("div#info p:nth-child(2)", "text").toString();
                String description = page.getHtml().css("div#intro p", "text").toString();
                String category = page.getHtml().css("div.con_top>a:nth-of-type(2)", "text").toString();
                //封装保存
                Novel novel = new Novel();
                novel.setImage(image);
                novel.setTitle(title);
                novel.setAuthor(autor);
                novel.setDescription(description);
                novel.setCategory(category);
                page.putField("novel", novel);
                //List<Selectable> chapterTitle = page.getHtml().css("div#list a", "text").nodes();//章节信息
                //logger.info("断点");
            } else {
                //(小说章节页)
                String novelTitle = page.getHtml().css("div.con_top>a:nth-of-type(3)", "text").toString();
                String chapterTitle = page.getHtml().css("h1", "text").toString();
                String content = page.getHtml().css("div#content").toString();//html格式
                //String content = page.getHtml().css("div#content","text").toString();
                //封装保存
                Novel novel = new Novel();
                novel.setTitle(novelTitle);
                Chapter chapter = new Chapter();
                chapter.setTitle(chapterTitle);
                Content content1 = new Content();
                content1.setDetails(content);
                NovelBO novelBO = new NovelBO();
                novelBO.setChapter(chapter);
                novelBO.setContent(content1);
                novelBO.setNovel(novel);
                page.putField("novelBO", novelBO);
                //logger.info("断点");
            }
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    /**
     * initialDelay 当任务启动后，等待多久执行方法
     * fixedDelay 没隔多久执行一次
     */
    @Scheduled(initialDelay = 1000, fixedDelay = 60 * 60 * 1000)
    public void process() {
        long startTime, endTime;
        logger.info("开始爬取.....");
        startTime = System.currentTimeMillis();
        Spider.create(new NovelProcessor())
                //设置爬取url
                .addUrl(url)
                //设置内存和去重
                .setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(100000)))
                //设置线程数
                .thread(20)
                //保存到MongoDB
                .addPipeline(novelPipeline)
                //开启
                .run();
        endTime = System.currentTimeMillis();
        logger.info("爬取结束.....耗时 : {}", ((endTime - startTime) / 1000));
    }
}
```

保存到MongoDB数据库中NovelPipeline
```
@Component
public class NovelPipeline implements Pipeline {

    private Logger logger = LoggerFactory.getLogger(NovelPipeline.class);

    @Autowired
    private NovelService novelService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private ContentService contentService;

    @Override
    public void process(ResultItems resultItems, Task task) {
        //判断数据是否为空
        Novel novel = resultItems.get("novel");
        NovelBO novelBO = resultItems.get("novelBO");
        //小说不为空保存
        if (novel != null) {
            novelService.save(novel);
            logger.info("保存小说《{}》成功",novel.getTitle());
        }
        if(novelBO != null){
            //先查询是那个小说
            List<Novel> tempNovels = novelService.findByTitle(novelBO.getNovel().getTitle());
            if(tempNovels != null){
                //小说不为空,更新第一本
                Novel tempNovel = tempNovels.get(0);
                //更新章节
                Chapter tempChapter = novelBO.getChapter();
                tempChapter.setNId(tempNovel.getId());
                Chapter saveChapter = chapterService.save(tempChapter);
                //更新内容
                Content tempContent = novelBO.getContent();
                tempContent.setCId(saveChapter.getId());
                contentService.save(tempContent);
                logger.info("更新《{}》--{}章节成功",tempNovel.getTitle(),tempChapter.getTitle());
            }else{
                //没有对应的小说
                logger.info("更新章节失败");
            }
        }
    }
}

```

**小说实体类Novel**
```
@Data
public class Novel {
    @Id
    private String id;
    /**
     * 小说名字
     */
    private String title;
    /**
     * 作者
     */
    private String author;
    /**
     * 简介
     */
    private String description;
    /**
     * 图片地址
     */
    private String image;
    /**
     * 类别
     */
    private String category;
}
```

**章节实体类Chapter**
```
@Data
public class Chapter {
    @Id
    private String id;
    /**
     * 章节标题
     */
    private String title;
    /**
     * 小说ID
     */
    private String nId;
}
```

**内容详情实体类Content**
```
@Data
public class Content {
    @Id
    private String id;
    /**
     * 内容
     */
    private String details;
    /**
     * 章节ID
     */
    private String cId;
}
```

**实体类NovelBO**
```
@Data
public class NovelBO {

    private Novel novel;

    private Chapter chapter;

    private Content content;
}
```

**DAO层NovelRepository**
```
public interface NovelRepository extends MongoRepository<Novel,String> {

    List<Novel> findByTitle(String title);

}
```

**service层NovelService NovelServiceImpl**
```
public interface NovelService {

        List<Novel> findAll();

        Novel findById(String id);

        List<Novel> findByTitle(String title);

        Novel save(Novel novel);

        void deleet(String id);

}

@Service
public class NovelServiceImpl implements NovelService {

    @Autowired
    private NovelRepository novelRepository;

    @Override
    public List<Novel> findAll() {
        return novelRepository.findAll();
    }

    @Override
    public Novel findById(String id) {
        return novelRepository.findById(id).get();
    }

    @Override
    public List<Novel> findByTitle(String title) {
        return novelRepository.findByTitle(title);
    }

    @Override
    public Novel save(Novel novel) {
        return novelRepository.save(novel);
    }

    @Override
    public void deleet(String id) {
        novelRepository.deleteById(id);
    }
}
```

**Controller层NovelController**
```
@Api(value = "小说Controller")
@CrossOrigin
@RestController
public class NovelController {

    @Autowired
    private NovelService novelService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private ContentService contentService;

    @ApiOperation(value = "获取所有小说",notes = "获取所有小说")
    @GetMapping("/novels")
    public ResultBean getNovelAll(){
        List<Novel> novels = novelService.findAll();
        if(novels != null){
            return ResultBean.success().add("novels",novels);
        }else{
            return ResultBean.fail();
        }
    }

    @ApiOperation(value = "根据小说ID获取所有章节",notes = "根据小说ID获取所有章节")
    @ApiImplicitParam(name = "id",value = "小说ID")
    @GetMapping("/chapters/{id}")
    public ResultBean getNovelChapters(@PathVariable("id") String id){
        List<Chapter> chapters = chapterService.findByNId(id);
        if(chapters != null){
            return ResultBean.success().add("chapters",chapters);
        }else{
            return ResultBean.fail();
        }
    }

    @ApiOperation(value = "根据章节id获取内容",notes = "根据章节id获取内容")
    @ApiImplicitParam(name = "id",value = "章节ID")
    @GetMapping("/content/{id}")
    public ResultBean getChapterContent(@PathVariable("id") String id){
        Content content = contentService.findByCId(id);
        if(content != null){
            return ResultBean.success().add("content",content);
        }else{
            return ResultBean.fail();
        }
    }

}

```

**通用返回结果类ResultBean**
```
@Data
public class ResultBean {

    private String code;
    private String msg;
    private Map<String,Object> body = new LinkedHashMap<>();

    public ResultBean add(String key, Object value){
        this.body.put(key,value);
        return this;
    }

    public static ResultBean success(){
        ResultBean re = new ResultBean();
        re.setCode("1");
        re.setMsg("成功");
        return re;
    }

    public static ResultBean fail(){
        ResultBean re = new ResultBean();
        re.setCode("-1");
        re.setMsg("失败");
        return re;
    }
}
```

### 省略部分dao层和service层，完整代码gitee，后台完成
![](readme_files/1.jpg)
![](readme_files/2.jpg)
### 前台采用uni-app，主要代码如下（完整代码gitee）

**列表页**
```
<template>
	<view>
		<view class="uni-list">
			<view class="uni-list-cell" hover-class="uni-list-cell-hover" v-for="(value, key) in listData" :key="key" @click="goDetail(value)">
				<view class="uni-media-list">
					<image class="uni-media-list-logo" :src="value.image"></image>

					<view class="uni-media-list-body">
						<view class="uni-media-list-text-top">{{ value.title }}</view>

						<view style="width: 216px;justify-content:space-between;display: flex;">
							<text class="author">{{ value.author }}</text>
							<text>{{ value.category }}</text>
						</view>

						<view class="uni-media-list-text-bottom description">
							<text >{{ value.description }}</text>
						</view>
					</view>
				</view>
			</view>
		</view>
		<uni-load-more :status="status" :content-text="contentText" />
	</view>
</template>

<script>
import uniLoadMore from '@/components/uni-load-more/uni-load-more.vue';
var dateUtils = require('@/common/util.js').dateUtils;
export default {
	components: {
		uniLoadMore
	},
	data() {
		return {
			listData: [],
			last_id: '',
			reload: false,
			status: 'more',
			contentText: {
				contentdown: '上拉加载更多',
				contentrefresh: '加载中',
				contentnomore: '没有更多'
			}
		};
	},
	onLoad() {
		this.getList();
	},
	onPullDownRefresh() {
		this.reload = true;
		this.last_id = '';
		this.getList();
	},
	onReachBottom() {
		this.status = 'more';
		this.getList();
	},
	methods: {
		getList() {
			var data = {
				column: 'id,title,author,description,image,category' //需要的字段名
			};
			if (this.last_id) {
				//说明已有数据，目前处于上拉加载
				this.status = 'loading';
				data.minId = this.last_id;
				data.time = new Date().getTime() + '';
				data.pageSize = 10;
			}
			uni.request({
				url: 'http://localhost:8080/novels',
				data: data,
				success: data => {
					if (data.statusCode == 200) {
						let list = this.setTime(data.data.body.novels);
						this.listData = this.reload ? list : this.listData.concat(list);
						this.last_id = list[list.length - 1].id;
						this.reload = false;
					}
				},
				fail: (data, code) => {
					console.log('fail' + JSON.stringify(data));
				}
			});
		},

		goDetail: function(e) {
			let detail = {
				author: e.author,
				description: e.description,
				id: e.id,
				image: e.image,
				category: e.category,
				title: e.title
			};
			uni.navigateTo({
				url: '../details/details?detail=' + encodeURIComponent(JSON.stringify(detail))
			});
		},

		setTime: function(items) {
			var novelsItems = [];
			items.forEach(e => {
				novelsItems.push({
					author: e.author,
					description: e.description,
					id: e.id,
					image: e.image,
					category: e.category,
					title: e.title
				});
			});
			return novelsItems;
		}
	}
};
</script>
```

**章节页**
```
<template>
	<view>
		<!-- 小说图片简介等 -->
		<view class="uni-list ">
			<view class="uni-media-list">
				<image class="uni-media-list-logo" :src="novel.image"></image>
		
				<view class="uni-media-list-body" style="height: 100px;">
					<view class="uni-media-list-text-top">{{novel.title}}</view>
		
					<view style="width: 216px;justify-content:space-between;display: flex;">
						<text >{{ novel.author }}</text>
						
					</view>
					
					<text >{{ novel.category }}</text>
					
					<view class="uni-media-list-text-bottom description">
						<text >{{ novel.description }}</text>
					</view>
				</view>
			</view>
			<!-- 
			<view class="" >
				
				<image class="uni-media-list-logo" :src="novel.image"  ></image>
				
				<view class="" >
					<view class="uni-media-list-text-top">{{novel.title}}</view>
					<view class=" uni-ellipsis">{{novel.category}}</view>
					<view class=" uni-ellipsis">{{novel.author}}</view>
					<view class=" uni-ellipsis">{{novel.description}}</view>
				</view>
			</view>
			  -->
		</view>
		<!-- 章节信息 -->
		<view class="text-box" style="text-align: center;" scroll-y="true">
                <text style="font-size: 20px;">所有章节</text>
            </view>
		<view class="uni-list">
			<view class="uni-list-cell" hover-class="uni-list-cell-hover" v-for="(item,index) in chapters" :key="index">
				<view class="uni-media-list">
					<view class="uni-media-list-body" @tap="toContent(item.id)">
						<view class="uni-media-list-text-top">{{item.title}}</view>
					</view>
				</view>
			</view>
		</view>
	</view>
</template>

<script>



export default {
	data() {
		return {
			novel:{},
			chapters:[]
		};
	},
	onLoad(event) {
		// TODO 后面把参数名替换成 payload
		const payload = event.detail || event.payload;
		// 目前在某些平台参数会被主动 decode，暂时这样处理。
		try {
			this.novel = JSON.parse(decodeURIComponent(payload));
		} catch (error) {
			this.novel = JSON.parse(payload);
		}
		uni.setNavigationBarTitle({
			title: this.novel.title
		});
		//根据小说id获取章节信息
		this.getDetail();
	},
	methods: {
		getDetail() {
			uni.request({
				url: 'http://localhost:8080/chapters/' + this.novel.id,
				success: data => {
					if (data.statusCode == 200) {
						this.chapters = data.data.body.chapters;
					}
				},
				fail: () => {
					console.log('fail');
				}
			});
		},
		toContent(id){
			// console.log("cesi"+id)
			uni.navigateTo({
				url: '../content/content?cid=' + id + "&novel=" + encodeURIComponent(JSON.stringify(this.novel))
			});
		}
	}
};
</script>

```

**详情页**
```
<template>
	<view class="uni-padding-wrap" style="margin-top: 20px;">
		<uParse :content="content.details" @preview="preview" @navigate="navigate" />
	</view>
</template>

<script>
	import uParse from '../../components/uParse/src/wxParse.vue'
	export default {
		components: {
			uParse
		},
		data() {
			return {
				cid:"",
				content:"",
				novel:{}
			}
		},
		onLoad(event) {
			// TODO 后面把参数名替换成 payload
			const cid = event.cid || event.payload;
			const novel = event.novel || event.payload;
			// 目前在某些平台参数会被主动 decode，暂时这样处理。
			try {
				this.novel = JSON.parse(decodeURIComponent(novel));
			} catch (error) {
				this.novel = JSON.parse(novel);
			}
			this.cid = cid;
			//题目
			uni.setNavigationBarTitle({
				title: this.novel.title
			});
			//根据小说id获取章节信息
			this.getDetail();
		},
		methods: {
			getDetail(){
				uni.request({
					url: 'http://localhost:8080/content/' + this.cid,
					success: data => {
						if (data.statusCode == 200) {
							this.content = data.data.body.content;
						}
					},
					fail: () => {
						console.log('fail');
					}
				});
			},
			preview(src, e) {
				// do something
				console.log("src: " + src);
			},
			navigate(href, e) {
				// 如允许点击超链接跳转，则应该打开一个新页面，并传入href，由新页面内嵌webview组件负责显示该链接内容
				console.log("href: " + href);
				uni.showModal({
					content : "点击链接为：" + href,
					showCancel:false
				})
			}
		}
	}
</script>

<style>
@import url("../../components/uParse/src/wxParse.css");
</style>
```

## 效果如下
![](readme_files/3.jpg)
![](readme_files/4.jpg)
![](readme_files/5.jpg)

## 至此一个简单的webmagic+uni-app的入门demo完成，完整代码